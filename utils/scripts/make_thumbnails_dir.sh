#!/bin/bash

export LANG=ja_JP.UTF-8
export LC_ALL=C

# このスクリプトがあるディレクトリ
pushd `dirname "$0"` > /dev/null
SCRIPTPATH=`pwd`
popd > /dev/null

# リポジトリルート
REPO_DIR="${SCRIPTPATH}/../../"

# サムネイル生成プログラム
MOGRIFY=mogrify
which $MOGRIFY >& /dev/null
if [ $? -ne 0 ]; then
    MOGRIFY="magick mogrify"
fi

# Windowsの環境変数入りパス対策
PATH=`echo $PATH | sed -e 's/:[^:]*%[^:]*//'`



# メイン処理


dir_list=($( \
    find . -type d -path '*/\.*' -prune -o -not -name '.*' -type f -iname '*.PSD' -printf '%h\n'  \
        | sort -u \
))

for i in "${dir_list[@]}"
do
    pushd "$i" > /dev/null
    pwd
    mkdir -p .thumbnails
    mkdir -p .previews
    rm -f ./thumbnails/*.jpg
    rm -f ./previews/*.jpg
    ${MOGRIFY} -path .thumbnails -flatten -format jpg -resize 320x -quality 70 "*.psd[0]" 
    ${MOGRIFY} -path .previews   -flatten -format jpg -resize 1280x -quality 90 "*.psd[0]"

    popd > /dev/null
done

