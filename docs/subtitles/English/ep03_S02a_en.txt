アカネ「……何かまた垂水先輩がいない気がするんですが……」
少佐「安心しろ、あれは趣味だ」
CIO「確か、列車の運転だったか、臨時で」
宏佳「ほぼ趣味ね」
結菜「戸ジメ、ヨシ。側灯、ヨシ。ホーム、ヨシ。出発相当、進行」

Yuina is absent today too, isn't she?

Don't worry about it. That's her hobby.

She's operating a train on a temporary basis, if I remember rightly.

It's almost a hobby.

Doors OK, Side lamps OK, Platform OK, Signals OK, and Go!