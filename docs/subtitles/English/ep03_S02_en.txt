＜S02＞
芽依「ううううう……パソコンが変なんです」
テロップ:京姫鉄道株式会社 広報部広報課長　余部静夫
余部「言った端から……」
芽依「システム課に連絡しないと……」
余部「……クビになるぞ、君も私も……あのう、敬川さん。すみません、今回の件……内々にしていただけませんか……？」
テロップ:京鉄ITソリューションズ株式会社 運用部基盤整備管理課端末保守係 係員 敬川康
敬川「いいですよ。次から気をつけてくださいね｣
敬川「駆除しておきました」
余部「ありがとうございました」
芽依「ありがとうございました」
芽依「ふう……」
余部「ふう……クビの皮が繋がった……。葛城取締役本気だからね。もうちょっとは自覚を持って、他人に迷惑をかけないようにしなさい」
芽依「はい……」
芽依「ううう……こんな日に限って」（→次カット）
芽依「ついてない……」
粟生(OFF)「ごめん、英賀保さん取ってー！」
芽依「（外向きの口調、少し涙声気味に）お電話ありがとうございます。京姫鉄道広報課　英賀保と申します」
朝霧（電話）「（芽依が言い終わる前に少し被せて）朝霧よしみちゅと申します」
朝霧「実はね、数分前から、うちのWEBサイトにね、おたくのIPアドレスからDoS攻撃らしき不審な通信が」
芽依（モノ）「え……？IP？どすこい？」
芽依「イタズラでした！」
朝霧（電話）「あーちょっとー！？」
芽依（モノ）「世の中暇人が多いですね。ま、少しは気分が変わったかも」
同僚「神戸テレビさん来られました」
芽依「はーい、行きまーす」

(Hmmmm....) The computer is acting weird.

Kyoki Railway Co., Ltd. 
Chief of Public Relations Department
Public Relations Division
YOBE Shizuo

It's what he's just said...

I have to make contact with System Division, or...

We'll be fired. Not only you but also I...
Excuse me, Mr.Uyagawa, could you keep this matter a secret?

Kyoki Railway IT Solutions Co., Ltd.
Operations Department
Infrastructure Development Division
Terminal Maintenance Unit Staff
UYAGAWA Yasu 

Okay. Please be careful from the next time.

I've got rid of the virus.

Thank you very much.

Thank you very much.

Phew.

Phew. We were about to be fired since Mr.Katsuragi is serious.
Act carefully and don't make trouble to other people.

Certainly...

(Oh, no.) What a day today!

I'm out of luck.

Mei, can you pick it up?

Thank you for calling. This is Agaho in Kyoki Railway Public Relations Division.

This is Yoshimitsu Asagiri.

Actually, from a few minutes ago, there are suspicious connections like Dos attacks to our website from your IP address

What? IP? "Dosukoi"?

It was a prank!

Hey, wait!!

I found there are people in the world who have time to waste. 
Well, that made me feel a little better.

Here comes Kobe Television.

Here I come!