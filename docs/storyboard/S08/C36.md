---
layout: storyboard-cut
scene: S08
cut: C36
length: 2.5
jira_cut: 
image_names:
    - S08_C36
---
<section class='action' markdown='1'>
アクション
===

* 条文

</section>

<section class='dialogue' markdown='1'>
セリフ
===

* 万能倉「『三時間以上本線における運転を支障する』から、両方じゃねえか？」  
* 山口「では、報告の根拠としては、鉄道事故等報告規則 第五条第一項第五号と、同第二項第一号とします」  
* 万能倉「サイバー攻撃の疑いもある。『特に異例と認められるもの』も加えとけ」

</section>