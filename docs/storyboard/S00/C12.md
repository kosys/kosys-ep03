---
layout: storyboard-cut
scene: S00
cut: C12
image_names:
 - C12-1
 - C12-2
 - C12-3
 - C12-4
length: 4
jira_cut: KOSYS03-162
---
<section class="action" markdown="1">
アクション
===

改札機の扉が閉じ、芽依は吹き飛ばされる。（コンテの各コマはだいたい1秒ぐらいを目安に）

</section>

<section class="dialogue" markdown="1">
セリフ
===

* SE: ピンポーン（改札機エラー音）
* SE: バタン（改札機の扉が閉じる音）
* 芽依「ごふっ……！」

</section>