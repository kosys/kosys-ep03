---
layout: storyboard-cut
scene: S00
cut: C01
length: 5
jira_cut: KOSYS03-151
---
<section class="action" markdown="1">
アクション
===
朝の通勤時間帯。季節は春。

明確なロケ地があるわけではないが、場所は京姫鉄道 [八重畑駅](https://goo.gl/maps/bTm9z79yZp22)-[豊国駅](https://goo.gl/maps/oNim6T1BcXJ2)間（いずれも架空の駅）のイメージ。
たとえば、[ここ](https://goo.gl/maps/Nc8rrVYJBHQ2)から北方面を見るアングルとか。いずれにしても、現実の風景をそこまで気にする必要はない。

２秒目ぐらいから、４両編成の電車が右から左に（姫路駅方面に）進んでいく。

電車は、京姫鉄道K133系電車（架空の電車）。<br />
デザインは[資料](https://file.opap.jp/ws-kosys-common/designs/train/KKR-K133?goto=kosys-common%2Fdesigns%2Ftrain%2FKKR-K133%2Ftrain.png)を参照。

散る桜は、OPのエフェクトを流用しても良いかも知れない。さらに、PAN DOWNのカメラワークを付ける。

</section>

<section class="dialogue" markdown="1">
セリフ
===
* BGM: ゆったりとしたBGM。ヒーリング系。
* SE: K133系電車　走行音（225系電車の走行音）


</section>