---
layout: storyboard-cut
scene: S11
cut: C20c
image_names:
 - C20c-1
 - C20c-2
length: 4
jira_cut: 
---
<section class='action' markdown='1'>
アクション
===


* 前カットに引き続き、真っ黒な背景に、赤い線画（撮影でグロー効果）のみ。BADUSBマンも赤い線画
* BAD USBマン（？）が、キーボードを入力する


</section>


<section class='dialogue' markdown='1'>
セリフ
===

* BADUSBマン「うっしっし」

</section>