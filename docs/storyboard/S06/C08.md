---
layout: storyboard-cut
scene: S06
cut: C08
length: 3.0
jira_cut: KOSYS03-342
---
<section class='action' markdown='1'>
アクション
===

客から吊し上げられる女性駅員。駅の柱に磔にされている。

列車が遅れて客から吊し上げに遭う駅員の心情を誇張して描く。ただし、リアルに描きすぎると痛々しくなってしまうため、あくまでもデフォルメ調のコメディタッチに。


</section>

<section class='dialogue' markdown='1'>
セリフ
===
駅員「どうなってるんだぁあああ」

</section>