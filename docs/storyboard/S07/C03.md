---
layout: storyboard-cut
scene: S07
cut: C03
length: 2.5
jira_cut: 
image_names:
    - S07_C03-1
    - S07_C03-2
---
<section class='action' markdown='1'>
アクション
===


</section>

<section class='dialogue' markdown='1'>
セリフ
===

* 宏佳「輸送管理系と情報系・業務系はネットワークは分離しているでしょ」
* 宏佳「両方のネットワークに接続しているのは、認証サーバーと端末向けのアップデート配信サーバーぐらい……それが、マルウェアに感染？」
* 少佐「もう一つ可能性があります。内部犯が故意に攻撃しているという可能性です」
* 宏佳「……！」

</section>