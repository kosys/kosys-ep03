---
layout: storyboard-cut
scene: S07
cut: C06
length: 5
jira_cut: 
image_names:
    - S07_C06
---
<section class='action' markdown='1'>
アクション
===

テロップ『京鉄ITソリューションズ株式会社　保守係長 夢前さくら』

</section>

<section class='dialogue' markdown='1'>
セリフ
===

* 夢前「ログですか？　保存期間はバラバラです。一部は、もう消えてるかも」

</section>