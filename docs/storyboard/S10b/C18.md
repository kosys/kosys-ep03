---
layout: storyboard-cut
scene: S10b
cut: C18
length: 3
jira_cut: 
image_names:
    - S10b_C18
---
<section class='action' markdown='1'>
アクション
===

* アカネの手、キーボードのUSBケーブルを抜く
* キーボードののScrollLockが消灯

</section>

<section class='dialogue' markdown='1'>
セリフ
===

* (SE)ピピピピピ止まる

</section>