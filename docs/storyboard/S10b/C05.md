---
layout: storyboard-cut
scene: S10b
cut: C05
length: 11
jira_cut: 
image_names:
    - S10b_C05
---
<section class='action' markdown='1'>
アクション
===

* 「ここをキャンプ地とする」的な雰囲気の筆文字風テロップで、セリフに合わせて「どすこいが」「IPで」「何ちゃらかんちゃら」「何ちゃら義満」というテロップを出す。（ライセンスの都合で使用できるフォントは見つからないので、手書きで再現する必要がある）
* アカネ、フキダシインサート

</section>

<section class='dialogue' markdown='1'>
セリフ
===

* 芽依「どすこいがIPで何ちゃらかんちゃらって電話があったんだよ。何ちゃら義満とかいう人から」

* アカネ「朝霧義満先生からですか！？ セキュリティ専門家の」


</section>