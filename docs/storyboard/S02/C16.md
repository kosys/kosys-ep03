---
layout: storyboard-cut
scene: S02
cut: C16
length: 9
image_names:
 - C16-1
 - C16-2
jira_cut: KOSYS03-77
---
<section class="action" markdown="1">
アクション
===
* 芽依は、気楽な笑みを浮かべる。（少し小首を傾げた感じで）
* 同僚に呼びかけられ、顔を正す。

</section>

<section class="dialogue" markdown="1">
セリフ
===

* 芽依（モノ）「世の中暇人が多いですね。ま、少しは気分が変わったかも」
* 同僚「神戸テレビさん来られました」
* 芽依「はーい、行きまーす」

</section>