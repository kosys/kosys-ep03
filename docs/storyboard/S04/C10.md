---
layout: storyboard-cut
scene: S04
cut: C10
length: 6.5
jira_cut: KOSYS03-294
---
<section class='action' markdown='1'>
アクション
===

宏佳が少佐の元に歩いてくる。半歩ぐらい歩きのアクションで、画面を覗き込む。

</section>

<section class='dialogue' markdown='1'>
セリフ
===

* 宏佳「ほかのシステムはどう？」
* 少佐「全体的に重めです。サーバーによってばらつきがありますが」


</section>